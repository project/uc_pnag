
-- SUMMARY --

This module integrates the payment method sofortueberweisung.de and directebanking.com
from Payment Network AG. It is plug-and-play and you can even create a user-account
and/or project on Payment Network from within your Drupal/Ubercart admin pages with all
the required settings.

More details about sofortueberweisung.de and directebanking.com http://www.payment-network.com

-- REQUIREMENTS --

* Drupal 6 and Ubercart module 2.0.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure UC_PNAG in Administer >> Store Administration >> Store Configuration >> Configure Payments

-- CUSTOMIZATION --

None

-- TROUBLESHOOTING --

Not known yet

-- FAQ --

Yet to come

-- CONTACT --

Current maintainer:
* J�rgen Haas (jurgenhaas) - http://drupal.org/user/168924

This project has been sponsored by:
* PARAGON Executive Services GmbH
  Providing IT services as individual as the requirements. Find out more
  from http://www.paragon-es.de
