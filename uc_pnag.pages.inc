<?php

/**
 * @file
 * Forms for the uc_pnag module.
 *
 */


/**
 *
 */
function uc_pnag_project_create() {
  global $base_url;
  $country = uc_get_country_data(array('country_id' => variable_get('uc_store_country', uc_pnag_DEFAULT_COUNTRY)));
  $country_id = $country[0]['country_iso_code_2'];
  $form = array();
  $data = array(
    'user_salutation' => '1',
    'user_name1' => variable_get('uc_store_owner', ''),
    'user_street' => variable_get('uc_store_street1', ''),
    'user_street_additive' => variable_get('uc_store_street2', ''),
    'user_zipcode' => variable_get('uc_store_postal_code', ''),
    'user_city' => variable_get('uc_store_city', ''),
    'user_country_id' => $country_id,
    'user_email' => variable_get('uc_store_email', variable_get('site_email', '')),
    'user_phone' => variable_get('uc_store_phone', ''),
    'user_fax' => variable_get('uc_store_fax', ''),
    'user_homepage' => $base_url,
    'user_shop_system_id' => uc_pnag_SHOP_SYSTEM_ID,
    'project_shop_system_id' => uc_pnag_SHOP_SYSTEM_ID,
    'project_hash_algorithm' => uc_pnag_HASH_ALGORITHM,
    'projectssetting_currency_id' => variable_get('uc_currency_code', 'EUR'),
    'projectssetting_interface_success_link' => _uc_pnag_url_post('payment_link_success') .'?order_id=-USER_VARIABLE_0-',
    'projectssetting_interface_success_link_redirect' => '1',
    'projectssetting_interface_cancel_link' => _uc_pnag_url_post('payment_link_cancel'),
    'projectssetting_interface_input_hash_check_enabled' => '1',
    'projectssetting_locked_amount' => '1',
    'projectssetting_locked_reason_1' => '1',
    'projectssetting_locked_reason_2' => '1',
    'projectssetting_interface_timeout' => '300',
    'projectssetting_interface_timeout_link' => _uc_pnag_url_post('payment_link_timeout'),
    'projectsnotification_email_activated' => '1',
    'projectsnotification_email_email' => variable_get('uc_store_email', variable_get('site_email', '')),
    'projectsnotification_email_language_id' => _uc_pnag_language(),
    'projectsnotification_http_activated' => '1',
    'projectsnotification_http_url' => _uc_pnag_url_post('payment_link_notify'),
    'projectsnotification_http_method' => '1',
    'projectsnotification_httpresponse_activated' => '0',
    'projectsnotification_httpresponse_url' => '',
    'projectsnotification_httpresponse_method' => '',
    'projectsnotification_httpresponse_text' => '',
    'backlink' => _uc_pnag_url_post('create_project_response'),
  );
  foreach ($data as $name => $value) {
    $form[$name] = array('#type' => 'hidden', '#value' => $value);
  }
  $form['user_legal_form_id'] = array(
    '#type' => 'select',
    '#title' => t('Legal form'),
    '#options' => uc_pnag_LEGAL_FORMS(),
    '#default_value' => uc_pnag_var('user_legal_form_id'),
  );
  $form['project_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Project name'),
    '#default_value' => 'Test-Projekt',
  );
  $form['projectssetting_project_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Project password'),
    '#default_value' => uc_pnag_var('password'),
  );
  $form['project_notification_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Project password for notifications'),
    '#default_value' => uc_pnag_var('password_notification'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create Project'),
  );
  $form['#validate'][] = 'uc_pnag_project_create_validate';
  return $form;
}

function uc_pnag_project_create_validate($form, &$form_state) {
  $pw = trim($form_state['values']['projectssetting_project_password']);
  if (empty($pw) || strlen($pw) < 6) {
    form_set_error('projectssetting_project_password', t('Password needs to be at least 6 characters long.'));
    $form_state['values']['projectssetting_project_password'] = $pw;
  }
  $pw = trim($form_state['values']['project_notification_password']);
  if (empty($pw) || strlen($pw) < 6) {
    form_set_error('project_notification_password', t('Password needs to be at least 6 characters long.'));
    $form_state['values']['project_notification_password'] = $pw;
  }
}

function uc_pnag_project_create_submit($form, &$form_state) {
  $url = _uc_pnag_url_post('create_project');
  $pw = $form_state['values']['projectssetting_project_password'];
  variable_set('uc_pnag_password', $pw);
  $pw = $form_state['values']['project_notification_password'];
  variable_set('uc_pnag_password_notification', $pw);
  $param = '';
  $delimiter = '';
  foreach ($form_state['values'] as $name => $value) {
    $param .= $delimiter . $name .'='. check_plain($value);
    $delimiter = '&';
  }
  drupal_goto($url, $param);
}

/**
 *
 */
function uc_pnag_project_create_complete() {
  variable_set('uc_pnag_user_id', check_plain($_GET['user_id']));
  variable_set('uc_pnag_project_id', check_plain($_GET['project_id']));
  $msg  = t('Your project has been successfullly created. The user ID is !user_id and the project ID is !project_id. These values have been saved in your local setting. To proceed with your payment settings please click !url.', array('!user_id' => uc_pnag_var('user_id'), '!project_id' => uc_pnag_var('project_id'), '!url' => l('here', 'admin/store/settings/payment/edit/methods'), ));
  drupal_set_message($msg);
}

/**
 *
 */
function _uc_pnag_project_form($user, $project) {
  $form = array();
  $form['uc_pnag_user_id'] = array(
    '#type' => 'textfield',
    '#title' => t('User ID'),
    '#default_value' => $user,
  );
  $form['uc_pnag_project_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Project ID'),
    '#default_value' => $project,
  );
  $form['uc_pnag_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Project password'),
    '#default_value' => uc_pnag_var('password'),
  );
  $form['uc_pnag_password_notification'] = array(
    '#type' => 'textfield',
    '#title' => t('Project password for notifications'),
    '#default_value' => uc_pnag_var('password_notification'),
  );
  return $form;
}

/**
 *
 */
function uc_pnag_payment_success() {
  $order_id = check_plain($_GET['order_id']);
  if (intval($_SESSION['cart_order']) != $order_id) {
    $_SESSION['cart_order'] = $order_id;
  }
  if (!($order = uc_order_load($order_id))) {
    drupal_goto('cart');
  }
  // This lets us know it's a legitimate access of the complete page.
  $_SESSION['do_complete'] = TRUE;
  drupal_goto('cart/checkout/complete');
}

/**
 *
 */
function uc_pnag_payment_cancel() {
  unset($_SESSION['cart_order']);
  drupal_set_message(t('Your payment was cancelled. Please feel free to continue shopping or contact us for assistance.'));
  drupal_goto('cart/checkout');
}

/**
 *
 */
function uc_pnag_payment_timeout() {
  $data = _uc_pnag_check_hash();
  watchdog('uc_pnag', t('Received timeout with POST data: %var'), array('%var' => print_r($data, TRUE)), WATCHDOG_ERROR);
}

/**
 *
 */
function uc_pnag_payment_notify() {
  $data = _uc_pnag_check_hash();
  if (!$data['verified']) {
    watchdog('uc_pnag', t('Received notification with invalid POST data: %var'), array('%var' => print_r($data, TRUE)), WATCHDOG_ERROR);
    print 'Notification not verified!';
    exit;
  }
  else {
    watchdog('uc_pnag', t('Received notification with POST data: %var'), array('%var' => print_r($data, TRUE)), WATCHDOG_WARNING);
  }
  $order_id = $data['user_variable_0'];
  $txn_id = $data['transaction'];
  $amount = $data['amount'];
  $currency = $data['currency_id'];
  $order = uc_order_load($order_id);
  if ($order == FALSE) {
    watchdog('uc_pnag', 'Notification attempted for non-existent order.', array(), WATCHDOG_ERROR);
    print 'Notification not successfull, order does not exist!';
    exit;
  }
  $context = array(
    'revision' => 'formatted-original',
    'location' => 'paypal-ipn',
  );
  $options = array(
    'sign' => FALSE,
  );
  $comment = t('PaymentNetwork transaction ID: @txn_id', array('@txn_id' => $txn_id));
  uc_payment_enter($order_id, 'PaymentNetwork', $amount, $order->uid, NULL, $comment);
  uc_order_comment_save($order_id, 0, t('Payment of @amount @currency submitted through Payment Network.', array('@amount' => uc_price($amount, $context, $options), '@currency' => $currency)), 'order', 'payment_received');
  uc_order_comment_save($order_id, 0, t('Payment Network reported a payment of @amount @currency.', array('@amount' => uc_price($amount, $context, $options), '@currency' => $currency)));
  print 'Notification successfull!';
  exit;
}

// Returns the form elements for the PaymentNetwork checkout form.
function uc_pnag_checkout_form($form_state, $order) {
  $context = array(
    'revision' => 'formatted-original',
    'location' => 'pnag-form',
  );
  $options = array(
    'sign' => FALSE,
    'thou' => FALSE,
    'dec' => '.',
  );
  $shipping = 0;
  foreach ($order->line_items as $item) {
    if ($item['type'] == 'shipping') {
      $shipping += $item['amount'];
    }
  }
  $tax = 0;
  if (module_exists('uc_taxes')) {
    foreach (uc_taxes_calculate($order) as $tax_item) {
      $tax += $tax_item->amount;
    }
  }
  $data = array(
    'user_id' => uc_pnag_var('user_id'),
    'project_id' => uc_pnag_var('project_id'),
    'amount' => uc_price($order->order_total, $context, $options),
    'currency_id' => variable_get('uc_currency_code', 'EUR'),
    'reason_1' => 'Order No. '. $order->order_id .'-'. $order->uid,
    'reason_2' => variable_get('uc_store_name', 'Online Shop'),
    'language_id' => _uc_pnag_language(),
    'sender_holder' => '',
    'sender_account_number' => '',
    'sender_bank_code' => '',
    'sender_country_id' => '',
    'user_variable_0' => $order->order_id,
    'user_variable_1' => '',
    'user_variable_2' => '',
    'user_variable_3' => '',
    'user_variable_4' => '',
    'user_variable_5' => 'Shop-System Name v.2.0',
  );
  $hashdata = array(
    $data['user_id'],
    $data['project_id'],
    $data['sender_holder'],
    $data['sender_account_number'],
    $data['sender_bank_code'],
    $data['sender_country_id'],
    $data['amount'],
    variable_get('uc_currency_code', 'EUR'),
    $data['reason_1'],
    $data['reason_2'],
    $data['user_variable_0'],
    $data['user_variable_1'],
    $data['user_variable_2'],
    $data['user_variable_3'],
    $data['user_variable_4'],
    $data['user_variable_5'],
    uc_pnag_var('password'),
  );
  $hashdata_implode = implode('|', $hashdata);
  $data['hash'] = sha1($hashdata_implode);
  $form['#action'] = _uc_pnag_url_post('start_payment');
  foreach ($data as $name => $value) {
    if (!empty($value)) {
      $form[$name] = array('#type' => 'hidden', '#value' => $value);
    }
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => _uc_pnag_submit(),
  );
  return $form;
}
