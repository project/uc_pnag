<?php

/**
 * @file
 * Parameter file for the uc_pnag module.
 *
 * In this file all system parameters from Payment Network are collected and
 * get returned to the module according to the current context.
 */

define('uc_pnag_SHOP_SYSTEM_ID', 190);
define('uc_pnag_HASH_ALGORITHM', 'sha1');
define('uc_pnag_DEFAULT_COUNTRY', 'DE');

function uc_pnag_COUNTRIES() {
  return array('DE','AT','CH','UK','NL','BE');
}

function uc_pnag_LANGUAGES() {
  return array(
    'de' => 'DE',
    'en' => 'UK',
    'nl' => 'NL',
  );
}

function uc_pnag_DOMAINS() {
  return array(
    'DE' => 'www.sofortueberweisung.de',
    'AT' => 'www.sofortueberweisung.at',
    'CH' => 'www.sofortueberweisung.ch',
    'UK' => 'www.directebanking.com',
    'NL' => 'www.directebanking.com',
    'BE' => 'www.directebanking.com',
  );
}

function uc_pnag_NAMES() {
  return array(
    'DE' => 'sofortüberweisung.de',
    'AT' => 'sofortüberweisung.at',
    'CH' => 'sofortüberweisung.ch',
    'UK' => 'DIRECTebanking.com',
    'NL' => 'DIRECTebanking.com',
    'BE' => 'DIRECTebanking.com',
  );
}

function uc_pnag_DESCRIPTIONS() {
  return array(
    'DE' => 'Online-Überweisung mit TÜV geprüftem Datenschutz ohne Registrierung. Bitte halten Sie Ihre Online-Banking-Daten (PIN/TAN) bereit. Dienstleistungen/Waren werden bei Verfügbarkeit SOFORT geliefert bzw. versendet!',
    'AT' => 'Online-Überweisung mit TÜV geprüftem Datenschutz ohne Registrierung. Bitte halten Sie Ihre Online-Banking-Daten (PIN/TAN) bereit. Dienstleistungen/Waren werden bei Verfügbarkeit SOFORT geliefert bzw. versendet!',
    'CH' => 'Online-Überweisung mit TÜV geprüftem Datenschutz ohne Registrierung. Bitte halten Sie Ihre Online-Banking-Daten (PIN/TAN) bereit. Dienstleistungen/Waren werden bei Verfügbarkeit SOFORT geliefert bzw. versendet!',
    'UK' => 'Online bank transfer without registration. TÜV certified data privacy. Please have your online banking details to hand. Available services/goods will be dispatched immediately.',
    'NL' => 'Online betalen zonder registratie! Beveiligd met een TÜV certificaat. Houd uw online bankgegevens (PIN/TAN) bij de hand. De dienst is direct beschikbaar of de goederen worden direct verstuurd!',
    'BE' => 'Online betalen zonder registratie! Beveiligd met een TÜV certificaat. Houd uw online bankgegevens (PIN/TAN) bij de hand. De dienst is direct beschikbaar of de goederen worden direct verstuurd!',
  );
}

function uc_pnag_LOGOS() {
  return array(
    'DE' => array(
      'path' => 'de',
      'logo_small'  => '100x38.png',
      'logo_medium' => '200x75.png',
      'logo_large'  => '320x120.png',
      'banner' => 'banner_200x68.png',
    ),
    'AT' => array(
      'path' => 'de',
      'logo_small'  => '100x38.png',
      'logo_medium' => '200x75.png',
      'logo_large'  => '320x120.png',
      'banner' => 'banner_200x68.png',
    ),
    'CH' => array(
      'path' => 'de',
      'logo_small'  => '100x38.png',
      'logo_medium' => '200x75.png',
      'logo_large'  => '320x120.png',
      'banner' => 'banner_200x68.png',
    ),
    'UK' => array(
      'path' => 'en',
      'logo_small'  => '100x38.png',
      'logo_medium' => '200x75.png',
      'logo_large'  => '320x120.png',
      'banner' => 'banner_300x100.png',
    ),
    'NL' => array(
      'path' => 'nl',
      'logo_small'  => '100x38.png',
      'logo_medium' => '200x75.png',
      'logo_large'  => '320x120.png',
      'banner' => 'banner_300x100.png',
    ),
    'BE' => array(
      'path' => 'nl',
      'logo_small'  => '100x38.png',
      'logo_medium' => '200x75.png',
      'logo_large'  => '320x120.png',
      'banner' => 'banner_300x100.png',
    ),
  );
}

function uc_pnag_LEGAL_FORMS() {
  return array(
    '1'  => 'Keine Rechtsform/Natürliche Person',
    '2'  => 'Nicht aufgeführte Rechtsform',
    '3'  => 'Selbständig',
    '4'  => 'Aktiengesellschaft',
    '5'  => 'Aktiengesellschaft & Co. KG',
    '6'  => 'AG & Co. OHG',
    '7'  => 'Genossenschaft',
    '8'  => 'Gesellschaft mit beschränkter Haftung',
    '9'  => 'Gesellschaft mit beschränkter Haftung & Co. KG',
    '10' => 'Gesellschaft mit beschränkter Haftung & Co. KEG',
    '11' => 'Gesellschaft mit beschränkter Haftung & Co. OHG',
    '12' => 'Gesellschaft nach bürgerlichem Recht',
    '13' => 'Kommanditerwerbsgesellschaft',
    '14' => 'Kommanditgesellschaft',
    '15' => 'Offene Erwerbsgesellschaft',
    '16' => 'Offene Handelsgesellschaft',
    '17' => 'Registrierte Genossenschaft mit beschränkter Haftung',
    '18' => 'Eingetragene Genossenschaft',
    '19' => 'Eingetragener Kaufmann',
    '20' => 'Eingetragener Verein',
    '21' => 'Limited',
    '22' => 'Public limited company',
    '23' => 'Societé anonymé',
    '24' => 'Societé à responsabilité limitée',
    '25' => 'Sociétas Europaéa',
  );
}

/**
 *
 */
function _uc_pnag_language() {
  static $_uc_pnag_lng;
  if (!isset($_uc_pnag_lng)) {
    global $language;
    $lng = $language->language;
    $lngs = uc_pnag_LANGUAGES();
    if (isset($lngs[$lng])) {
      $_uc_pnag_lng = $lngs[$lng];
    }
    else {
      $_uc_pnag_lng = uc_pnag_DEFAULT_COUNTRY;
    }
  }
  return $_uc_pnag_lng;
}

/**
 *
 */
function _uc_pnag_domain() {
  static $_uc_pnag_domain;
  if (!isset($_uc_pnag_domain)) {
    $l = uc_pnag_DOMAINS();
    $_uc_pnag_domain = $l[_uc_pnag_language()];
  }
  return $_uc_pnag_domain;
}

/**
 *
 */
function _uc_pnag_url_base() {
  return 'https://'. _uc_pnag_domain();
}

/**
 *
 */
function _uc_pnag_url_post($type) {
  global $base_url;
  $url = _uc_pnag_url_base();
  switch ($type) {
    case 'create_project':
      return $url .'/payment/createNew/';
    case 'start_payment':
      return $url .'/payment/start';
    default:
      return $base_url .'/'. _uc_pnag_path_post($type);
  }
}

/**
 *
 */
function _uc_pnag_path_post($type) {
  switch ($type) {
    case 'create_project_form':
      return 'admin/store/settings/payment/uc_pnag/project/create';
    case 'create_project_response':
      return 'uc_pnag/project/create/completed';
    case 'payment_link_success':
      return 'cart/uc_pnag/payment/success';
    case 'payment_link_cancel':
      return 'cart/uc_pnag/payment/cancel';
    case 'payment_link_timeout':
      return 'cart/uc_pnag/payment/timeout';
    case 'payment_link_notify':
      return 'cart/uc_pnag/payment/notify';
  }
}

/**
 *
 */
function _uc_pnag_check_hash() {
  $data = array(
    'transaction' => check_plain($_POST['transaction']),
    'user_id' => check_plain($_POST['user_id']),
    'project_id' => check_plain($_POST['project_id']),
    'sender_holder' => check_plain($_POST['sender_holder']),
    'sender_account_number' => check_plain($_POST['sender_account_number']),
    'sender_bank_code' => check_plain($_POST['sender_bank_code']),
    'sender_bank_name' => check_plain($_POST['sender_bank_name']),
    'sender_bank_bic' => check_plain($_POST['sender_bank_bic']),
    'sender_iban' => check_plain($_POST['sender_iban']),
    'sender_country_id' => check_plain($_POST['sender_country_id']),
    'recipient_holder' => check_plain($_POST['recipient_holder']),
    'recipient_account_number' => check_plain($_POST['recipient_account_number']),
    'recipient_bank_code' => check_plain($_POST['recipient_bank_code']),
    'recipient_bank_name' => check_plain($_POST['recipient_bank_name']),
    'recipient_bank_bic' => check_plain($_POST['recipient_bank_bic']),
    'recipient_iban' => check_plain($_POST['recipient_iban']),
    'recipient_country_id' => check_plain($_POST['recipient_country_id']),
    'international_transaction' => check_plain($_POST['international_transaction']),
    'amount' => check_plain($_POST['amount']),
    'currency_id' => check_plain($_POST['currency_id']),
    'reason_1' => check_plain($_POST['reason_1']),
    'reason_2' => check_plain($_POST['reason_2']),
    'security_criteria' => check_plain($_POST['security_criteria']),
    'user_variable_0' => check_plain($_POST['user_variable_0']),
    'user_variable_1' => check_plain($_POST['user_variable_1']),
    'user_variable_2' => check_plain($_POST['user_variable_2']),
    'user_variable_3' => check_plain($_POST['user_variable_3']),
    'user_variable_4' => check_plain($_POST['user_variable_4']),
    'user_variable_5' => check_plain($_POST['user_variable_5']),
    'created' => check_plain($_POST['created']),
    'project_password' => uc_pnag_var('password_notification'),
  );
  $hash_check = check_plain($_POST['hash']);
  $data_implode = implode('|', $data);
  $hash = sha1($data_implode);
  if ($hash == $hash_check) {
    $data['verified'] = TRUE;
  }
  else {
    $data['verified'] = FALSE;
  }
  return $data;
}

/**
 *
 */
function _uc_pnag_name() {
  $lng = _uc_pnag_language();
  $names = uc_pnag_NAMES();
  return $names[$lng];
}

/**
 *
 */
function _uc_pnag_logo() {
  $lng   = _uc_pnag_language();
  $logos = uc_pnag_LOGOS();
  $logo  = url(drupal_get_path('module', 'uc_pnag') .'/ressources/'. $logos[$lng]['path'] .'/'. $logos[$lng]['logo_small'], array('absolute' => TRUE, 'external' => TRUE));
  $alt   = _uc_pnag_name();
  $title = t('Payment method !name.', array('!name' => _uc_pnag_name(),));
  return theme_image($logo, $alt, $title, array(), FALSE);
}

/**
 *
 */
function _uc_pnag_name_logo() {
  return _uc_pnag_logo();
}

/**
 *
 */
function _uc_pnag_submit() {
  return t('Submit order');
}

/**
 *
 */
function _uc_pnag_description() {
  $lng = _uc_pnag_language();
  $desc = uc_pnag_DESCRIPTIONS();
  return _uc_pnag_logo() .'<div>'. $desc[$lng] .'</div>';
}
